#!/bin/bash
# $1 = install or clean

NUMJOBS=`nproc --all`
ARG=$1

if [ -z "$1" ]; then
echo "Please tell what you wanna do [install|clean]"
ARG="install"
fi

cd platform/EOSM.202
make $ARG -j$NUMJOBS INSTALL_DIR=$PWD/ML INSTALL_FINISH="" CONFIG_PTP=y CONFIG_PTP_ML=y ML_MODULES="adtg_gui mlv_lite mlv_play mlv_snd dual_iso silent lua crop_rec sd_uhs file_man bench" PREFERRED_ARM_PATH=/home/cedric/apps/gcc-arm-none-eabi-5_4-2016q3
