################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/mlv_rec/raw_proc/chroma_smooth.c \
../modules/mlv_rec/raw_proc/histogram.c \
../modules/mlv_rec/raw_proc/patternnoise.c \
../modules/mlv_rec/raw_proc/pixel_proc.c \
../modules/mlv_rec/raw_proc/stripes.c 

OBJS += \
./modules/mlv_rec/raw_proc/chroma_smooth.o \
./modules/mlv_rec/raw_proc/histogram.o \
./modules/mlv_rec/raw_proc/patternnoise.o \
./modules/mlv_rec/raw_proc/pixel_proc.o \
./modules/mlv_rec/raw_proc/stripes.o 

C_DEPS += \
./modules/mlv_rec/raw_proc/chroma_smooth.d \
./modules/mlv_rec/raw_proc/histogram.d \
./modules/mlv_rec/raw_proc/patternnoise.d \
./modules/mlv_rec/raw_proc/pixel_proc.d \
./modules/mlv_rec/raw_proc/stripes.d 


# Each subdirectory must supply rules for building sources it contributes
modules/mlv_rec/raw_proc/%.o: ../modules/mlv_rec/raw_proc/%.c modules/mlv_rec/raw_proc/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


