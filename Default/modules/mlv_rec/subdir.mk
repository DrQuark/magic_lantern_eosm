################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/mlv_rec/dng2raw.c \
../modules/mlv_rec/lj92.c \
../modules/mlv_rec/mlv.c \
../modules/mlv_rec/mlv_dump.c \
../modules/mlv_rec/mlv_rec.c 

O_SRCS += \
../modules/mlv_rec/mlv.o 

OBJS += \
./modules/mlv_rec/dng2raw.o \
./modules/mlv_rec/lj92.o \
./modules/mlv_rec/mlv.o \
./modules/mlv_rec/mlv_dump.o \
./modules/mlv_rec/mlv_rec.o 

C_DEPS += \
./modules/mlv_rec/dng2raw.d \
./modules/mlv_rec/lj92.d \
./modules/mlv_rec/mlv.d \
./modules/mlv_rec/mlv_dump.d \
./modules/mlv_rec/mlv_rec.d 


# Each subdirectory must supply rules for building sources it contributes
modules/mlv_rec/%.o: ../modules/mlv_rec/%.c modules/mlv_rec/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


