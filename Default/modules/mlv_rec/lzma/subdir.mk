################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/mlv_rec/lzma/7zAlloc.c \
../modules/mlv_rec/lzma/7zBuf.c \
../modules/mlv_rec/lzma/7zBuf2.c \
../modules/mlv_rec/lzma/7zCrc.c \
../modules/mlv_rec/lzma/7zCrcOpt.c \
../modules/mlv_rec/lzma/7zDec.c \
../modules/mlv_rec/lzma/7zFile.c \
../modules/mlv_rec/lzma/7zIn.c \
../modules/mlv_rec/lzma/7zStream.c \
../modules/mlv_rec/lzma/Alloc.c \
../modules/mlv_rec/lzma/Bcj2.c \
../modules/mlv_rec/lzma/Bra.c \
../modules/mlv_rec/lzma/Bra86.c \
../modules/mlv_rec/lzma/BraIA64.c \
../modules/mlv_rec/lzma/CpuArch.c \
../modules/mlv_rec/lzma/Delta.c \
../modules/mlv_rec/lzma/LzFind.c \
../modules/mlv_rec/lzma/LzFindMt.c \
../modules/mlv_rec/lzma/Lzma2Dec.c \
../modules/mlv_rec/lzma/Lzma2Enc.c \
../modules/mlv_rec/lzma/Lzma86Dec.c \
../modules/mlv_rec/lzma/Lzma86Enc.c \
../modules/mlv_rec/lzma/LzmaDec.c \
../modules/mlv_rec/lzma/LzmaEnc.c \
../modules/mlv_rec/lzma/LzmaLib.c \
../modules/mlv_rec/lzma/MtCoder.c \
../modules/mlv_rec/lzma/Ppmd7.c \
../modules/mlv_rec/lzma/Ppmd7Dec.c \
../modules/mlv_rec/lzma/Ppmd7Enc.c \
../modules/mlv_rec/lzma/Sha256.c \
../modules/mlv_rec/lzma/Threads.c \
../modules/mlv_rec/lzma/Xz.c \
../modules/mlv_rec/lzma/XzCrc64.c \
../modules/mlv_rec/lzma/XzDec.c \
../modules/mlv_rec/lzma/XzEnc.c \
../modules/mlv_rec/lzma/XzIn.c 

OBJS += \
./modules/mlv_rec/lzma/7zAlloc.o \
./modules/mlv_rec/lzma/7zBuf.o \
./modules/mlv_rec/lzma/7zBuf2.o \
./modules/mlv_rec/lzma/7zCrc.o \
./modules/mlv_rec/lzma/7zCrcOpt.o \
./modules/mlv_rec/lzma/7zDec.o \
./modules/mlv_rec/lzma/7zFile.o \
./modules/mlv_rec/lzma/7zIn.o \
./modules/mlv_rec/lzma/7zStream.o \
./modules/mlv_rec/lzma/Alloc.o \
./modules/mlv_rec/lzma/Bcj2.o \
./modules/mlv_rec/lzma/Bra.o \
./modules/mlv_rec/lzma/Bra86.o \
./modules/mlv_rec/lzma/BraIA64.o \
./modules/mlv_rec/lzma/CpuArch.o \
./modules/mlv_rec/lzma/Delta.o \
./modules/mlv_rec/lzma/LzFind.o \
./modules/mlv_rec/lzma/LzFindMt.o \
./modules/mlv_rec/lzma/Lzma2Dec.o \
./modules/mlv_rec/lzma/Lzma2Enc.o \
./modules/mlv_rec/lzma/Lzma86Dec.o \
./modules/mlv_rec/lzma/Lzma86Enc.o \
./modules/mlv_rec/lzma/LzmaDec.o \
./modules/mlv_rec/lzma/LzmaEnc.o \
./modules/mlv_rec/lzma/LzmaLib.o \
./modules/mlv_rec/lzma/MtCoder.o \
./modules/mlv_rec/lzma/Ppmd7.o \
./modules/mlv_rec/lzma/Ppmd7Dec.o \
./modules/mlv_rec/lzma/Ppmd7Enc.o \
./modules/mlv_rec/lzma/Sha256.o \
./modules/mlv_rec/lzma/Threads.o \
./modules/mlv_rec/lzma/Xz.o \
./modules/mlv_rec/lzma/XzCrc64.o \
./modules/mlv_rec/lzma/XzDec.o \
./modules/mlv_rec/lzma/XzEnc.o \
./modules/mlv_rec/lzma/XzIn.o 

C_DEPS += \
./modules/mlv_rec/lzma/7zAlloc.d \
./modules/mlv_rec/lzma/7zBuf.d \
./modules/mlv_rec/lzma/7zBuf2.d \
./modules/mlv_rec/lzma/7zCrc.d \
./modules/mlv_rec/lzma/7zCrcOpt.d \
./modules/mlv_rec/lzma/7zDec.d \
./modules/mlv_rec/lzma/7zFile.d \
./modules/mlv_rec/lzma/7zIn.d \
./modules/mlv_rec/lzma/7zStream.d \
./modules/mlv_rec/lzma/Alloc.d \
./modules/mlv_rec/lzma/Bcj2.d \
./modules/mlv_rec/lzma/Bra.d \
./modules/mlv_rec/lzma/Bra86.d \
./modules/mlv_rec/lzma/BraIA64.d \
./modules/mlv_rec/lzma/CpuArch.d \
./modules/mlv_rec/lzma/Delta.d \
./modules/mlv_rec/lzma/LzFind.d \
./modules/mlv_rec/lzma/LzFindMt.d \
./modules/mlv_rec/lzma/Lzma2Dec.d \
./modules/mlv_rec/lzma/Lzma2Enc.d \
./modules/mlv_rec/lzma/Lzma86Dec.d \
./modules/mlv_rec/lzma/Lzma86Enc.d \
./modules/mlv_rec/lzma/LzmaDec.d \
./modules/mlv_rec/lzma/LzmaEnc.d \
./modules/mlv_rec/lzma/LzmaLib.d \
./modules/mlv_rec/lzma/MtCoder.d \
./modules/mlv_rec/lzma/Ppmd7.d \
./modules/mlv_rec/lzma/Ppmd7Dec.d \
./modules/mlv_rec/lzma/Ppmd7Enc.d \
./modules/mlv_rec/lzma/Sha256.d \
./modules/mlv_rec/lzma/Threads.d \
./modules/mlv_rec/lzma/Xz.d \
./modules/mlv_rec/lzma/XzCrc64.d \
./modules/mlv_rec/lzma/XzDec.d \
./modules/mlv_rec/lzma/XzEnc.d \
./modules/mlv_rec/lzma/XzIn.d 


# Each subdirectory must supply rules for building sources it contributes
modules/mlv_rec/lzma/%.o: ../modules/mlv_rec/lzma/%.c modules/mlv_rec/lzma/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


