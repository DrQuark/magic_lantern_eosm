################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/dual_iso/adobedng-bridge.c \
../modules/dual_iso/amaze-port.c \
../modules/dual_iso/amaze_demosaic_RT.c \
../modules/dual_iso/chroma_smooth.c \
../modules/dual_iso/cr2hdr.c \
../modules/dual_iso/dcraw-bridge.c \
../modules/dual_iso/dither.c \
../modules/dual_iso/dual_iso.c \
../modules/dual_iso/exiftool-bridge.c \
../modules/dual_iso/kelvin.c \
../modules/dual_iso/sleefsseavx.c \
../modules/dual_iso/timing.c 

O_SRCS += \
../modules/dual_iso/dual_iso.o 

OBJS += \
./modules/dual_iso/adobedng-bridge.o \
./modules/dual_iso/amaze-port.o \
./modules/dual_iso/amaze_demosaic_RT.o \
./modules/dual_iso/chroma_smooth.o \
./modules/dual_iso/cr2hdr.o \
./modules/dual_iso/dcraw-bridge.o \
./modules/dual_iso/dither.o \
./modules/dual_iso/dual_iso.o \
./modules/dual_iso/exiftool-bridge.o \
./modules/dual_iso/kelvin.o \
./modules/dual_iso/sleefsseavx.o \
./modules/dual_iso/timing.o 

C_DEPS += \
./modules/dual_iso/adobedng-bridge.d \
./modules/dual_iso/amaze-port.d \
./modules/dual_iso/amaze_demosaic_RT.d \
./modules/dual_iso/chroma_smooth.d \
./modules/dual_iso/cr2hdr.d \
./modules/dual_iso/dcraw-bridge.d \
./modules/dual_iso/dither.d \
./modules/dual_iso/dual_iso.d \
./modules/dual_iso/exiftool-bridge.d \
./modules/dual_iso/kelvin.d \
./modules/dual_iso/sleefsseavx.d \
./modules/dual_iso/timing.d 


# Each subdirectory must supply rules for building sources it contributes
modules/dual_iso/%.o: ../modules/dual_iso/%.c modules/dual_iso/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


