################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/lua/lua/lapi.c \
../modules/lua/lua/lauxlib.c \
../modules/lua/lua/lbaselib.c \
../modules/lua/lua/lbitlib.c \
../modules/lua/lua/lcode.c \
../modules/lua/lua/lcorolib.c \
../modules/lua/lua/lctype.c \
../modules/lua/lua/ldblib.c \
../modules/lua/lua/ldebug.c \
../modules/lua/lua/ldo.c \
../modules/lua/lua/ldump.c \
../modules/lua/lua/lfunc.c \
../modules/lua/lua/lgc.c \
../modules/lua/lua/linit.c \
../modules/lua/lua/liolib.c \
../modules/lua/lua/llex.c \
../modules/lua/lua/lmathlib.c \
../modules/lua/lua/lmem.c \
../modules/lua/lua/loadlib.c \
../modules/lua/lua/lobject.c \
../modules/lua/lua/lopcodes.c \
../modules/lua/lua/loslib.c \
../modules/lua/lua/lparser.c \
../modules/lua/lua/lstate.c \
../modules/lua/lua/lstring.c \
../modules/lua/lua/lstrlib.c \
../modules/lua/lua/ltable.c \
../modules/lua/lua/ltablib.c \
../modules/lua/lua/ltm.c \
../modules/lua/lua/lua.c \
../modules/lua/lua/luac.c \
../modules/lua/lua/lundump.c \
../modules/lua/lua/lutf8lib.c \
../modules/lua/lua/lvm.c \
../modules/lua/lua/lzio.c \
../modules/lua/lua/ml-lua-shim.c 

O_SRCS += \
../modules/lua/lua/lapi.o \
../modules/lua/lua/lauxlib.o \
../modules/lua/lua/lbaselib.o \
../modules/lua/lua/lbitlib.o \
../modules/lua/lua/lcode.o \
../modules/lua/lua/lcorolib.o \
../modules/lua/lua/lctype.o \
../modules/lua/lua/ldblib.o \
../modules/lua/lua/ldebug.o \
../modules/lua/lua/ldo.o \
../modules/lua/lua/ldump.o \
../modules/lua/lua/lfunc.o \
../modules/lua/lua/lgc.o \
../modules/lua/lua/linit.o \
../modules/lua/lua/liolib.o \
../modules/lua/lua/llex.o \
../modules/lua/lua/lmathlib.o \
../modules/lua/lua/lmem.o \
../modules/lua/lua/loadlib.o \
../modules/lua/lua/lobject.o \
../modules/lua/lua/lopcodes.o \
../modules/lua/lua/lparser.o \
../modules/lua/lua/lstate.o \
../modules/lua/lua/lstring.o \
../modules/lua/lua/lstrlib.o \
../modules/lua/lua/ltable.o \
../modules/lua/lua/ltablib.o \
../modules/lua/lua/ltm.o \
../modules/lua/lua/lundump.o \
../modules/lua/lua/lutf8lib.o \
../modules/lua/lua/lvm.o \
../modules/lua/lua/lzio.o \
../modules/lua/lua/ml-lua-shim.o 

OBJS += \
./modules/lua/lua/lapi.o \
./modules/lua/lua/lauxlib.o \
./modules/lua/lua/lbaselib.o \
./modules/lua/lua/lbitlib.o \
./modules/lua/lua/lcode.o \
./modules/lua/lua/lcorolib.o \
./modules/lua/lua/lctype.o \
./modules/lua/lua/ldblib.o \
./modules/lua/lua/ldebug.o \
./modules/lua/lua/ldo.o \
./modules/lua/lua/ldump.o \
./modules/lua/lua/lfunc.o \
./modules/lua/lua/lgc.o \
./modules/lua/lua/linit.o \
./modules/lua/lua/liolib.o \
./modules/lua/lua/llex.o \
./modules/lua/lua/lmathlib.o \
./modules/lua/lua/lmem.o \
./modules/lua/lua/loadlib.o \
./modules/lua/lua/lobject.o \
./modules/lua/lua/lopcodes.o \
./modules/lua/lua/loslib.o \
./modules/lua/lua/lparser.o \
./modules/lua/lua/lstate.o \
./modules/lua/lua/lstring.o \
./modules/lua/lua/lstrlib.o \
./modules/lua/lua/ltable.o \
./modules/lua/lua/ltablib.o \
./modules/lua/lua/ltm.o \
./modules/lua/lua/lua.o \
./modules/lua/lua/luac.o \
./modules/lua/lua/lundump.o \
./modules/lua/lua/lutf8lib.o \
./modules/lua/lua/lvm.o \
./modules/lua/lua/lzio.o \
./modules/lua/lua/ml-lua-shim.o 

C_DEPS += \
./modules/lua/lua/lapi.d \
./modules/lua/lua/lauxlib.d \
./modules/lua/lua/lbaselib.d \
./modules/lua/lua/lbitlib.d \
./modules/lua/lua/lcode.d \
./modules/lua/lua/lcorolib.d \
./modules/lua/lua/lctype.d \
./modules/lua/lua/ldblib.d \
./modules/lua/lua/ldebug.d \
./modules/lua/lua/ldo.d \
./modules/lua/lua/ldump.d \
./modules/lua/lua/lfunc.d \
./modules/lua/lua/lgc.d \
./modules/lua/lua/linit.d \
./modules/lua/lua/liolib.d \
./modules/lua/lua/llex.d \
./modules/lua/lua/lmathlib.d \
./modules/lua/lua/lmem.d \
./modules/lua/lua/loadlib.d \
./modules/lua/lua/lobject.d \
./modules/lua/lua/lopcodes.d \
./modules/lua/lua/loslib.d \
./modules/lua/lua/lparser.d \
./modules/lua/lua/lstate.d \
./modules/lua/lua/lstring.d \
./modules/lua/lua/lstrlib.d \
./modules/lua/lua/ltable.d \
./modules/lua/lua/ltablib.d \
./modules/lua/lua/ltm.d \
./modules/lua/lua/lua.d \
./modules/lua/lua/luac.d \
./modules/lua/lua/lundump.d \
./modules/lua/lua/lutf8lib.d \
./modules/lua/lua/lvm.d \
./modules/lua/lua/lzio.d \
./modules/lua/lua/ml-lua-shim.d 


# Each subdirectory must supply rules for building sources it contributes
modules/lua/lua/%.o: ../modules/lua/lua/%.c modules/lua/lua/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


