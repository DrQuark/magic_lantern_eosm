################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/lua/lua.c \
../modules/lua/lua_battery.c \
../modules/lua/lua_camera.c \
../modules/lua/lua_console.c \
../modules/lua/lua_constants.c \
../modules/lua/lua_display.c \
../modules/lua/lua_dryos.c \
../modules/lua/lua_globals.c \
../modules/lua/lua_interval.c \
../modules/lua/lua_key.c \
../modules/lua/lua_lens.c \
../modules/lua/lua_lv.c \
../modules/lua/lua_menu.c \
../modules/lua/lua_movie.c \
../modules/lua/lua_property.c \
../modules/lua/lua_task.c 

O_SRCS += \
../modules/lua/__ltostr.o \
../modules/lua/__v_printf.o \
../modules/lua/clearerr.o \
../modules/lua/errlist.o \
../modules/lua/errlistu.o \
../modules/lua/errno.o \
../modules/lua/errno_location.o \
../modules/lua/fclose.o \
../modules/lua/fdglue.o \
../modules/lua/fdglue2.o \
../modules/lua/feof.o \
../modules/lua/ferror.o \
../modules/lua/fflush.o \
../modules/lua/fgetc_unlocked.o \
../modules/lua/fgets.o \
../modules/lua/fopen.o \
../modules/lua/fprintf.o \
../modules/lua/fputc_unlocked.o \
../modules/lua/fputs.o \
../modules/lua/fread.o \
../modules/lua/freopen.o \
../modules/lua/fseek.o \
../modules/lua/ftell.o \
../modules/lua/fwrite.o \
../modules/lua/localeconv.o \
../modules/lua/lua.o \
../modules/lua/lua_battery.o \
../modules/lua/lua_camera.o \
../modules/lua/lua_console.o \
../modules/lua/lua_constants.o \
../modules/lua/lua_display.o \
../modules/lua/lua_dryos.o \
../modules/lua/lua_globals.o \
../modules/lua/lua_interval.o \
../modules/lua/lua_key.o \
../modules/lua/lua_lens.o \
../modules/lua/lua_lv.o \
../modules/lua/lua_menu.o \
../modules/lua/lua_movie.o \
../modules/lua/lua_property.o \
../modules/lua/lua_task.o \
../modules/lua/memchr.o \
../modules/lua/printf.o \
../modules/lua/setvbuf.o \
../modules/lua/stderr.o \
../modules/lua/stdin.o \
../modules/lua/stdout.o \
../modules/lua/strerror.o \
../modules/lua/strpbrk.o \
../modules/lua/strspn.o \
../modules/lua/strstr.o \
../modules/lua/strtof.o \
../modules/lua/strtol.o \
../modules/lua/ungetc.o \
../modules/lua/vfprintf.o \
../modules/lua/vprintf.o 

OBJS += \
./modules/lua/lua.o \
./modules/lua/lua_battery.o \
./modules/lua/lua_camera.o \
./modules/lua/lua_console.o \
./modules/lua/lua_constants.o \
./modules/lua/lua_display.o \
./modules/lua/lua_dryos.o \
./modules/lua/lua_globals.o \
./modules/lua/lua_interval.o \
./modules/lua/lua_key.o \
./modules/lua/lua_lens.o \
./modules/lua/lua_lv.o \
./modules/lua/lua_menu.o \
./modules/lua/lua_movie.o \
./modules/lua/lua_property.o \
./modules/lua/lua_task.o 

C_DEPS += \
./modules/lua/lua.d \
./modules/lua/lua_battery.d \
./modules/lua/lua_camera.d \
./modules/lua/lua_console.d \
./modules/lua/lua_constants.d \
./modules/lua/lua_display.d \
./modules/lua/lua_dryos.d \
./modules/lua/lua_globals.d \
./modules/lua/lua_interval.d \
./modules/lua/lua_key.d \
./modules/lua/lua_lens.d \
./modules/lua/lua_lv.d \
./modules/lua/lua_menu.d \
./modules/lua/lua_movie.d \
./modules/lua/lua_property.d \
./modules/lua/lua_task.d 


# Each subdirectory must supply rules for building sources it contributes
modules/lua/%.o: ../modules/lua/%.c modules/lua/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


