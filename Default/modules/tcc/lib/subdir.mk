################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/tcc/lib/bcheck.c \
../modules/tcc/lib/libtcc1.c 

S_UPPER_SRCS += \
../modules/tcc/lib/alloca86-bt.S \
../modules/tcc/lib/alloca86.S \
../modules/tcc/lib/alloca86_64.S 

OBJS += \
./modules/tcc/lib/alloca86-bt.o \
./modules/tcc/lib/alloca86.o \
./modules/tcc/lib/alloca86_64.o \
./modules/tcc/lib/bcheck.o \
./modules/tcc/lib/libtcc1.o 

C_DEPS += \
./modules/tcc/lib/bcheck.d \
./modules/tcc/lib/libtcc1.d 


# Each subdirectory must supply rules for building sources it contributes
modules/tcc/lib/%.o: ../modules/tcc/lib/%.S modules/tcc/lib/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

modules/tcc/lib/%.o: ../modules/tcc/lib/%.c modules/tcc/lib/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


