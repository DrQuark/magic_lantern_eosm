################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/tcc/arm-gen.c \
../modules/tcc/c67-gen.c \
../modules/tcc/conftest.c \
../modules/tcc/i386-asm.c \
../modules/tcc/i386-gen.c \
../modules/tcc/il-gen.c \
../modules/tcc/libtcc.c \
../modules/tcc/tcc.c \
../modules/tcc/tccasm.c \
../modules/tcc/tcccoff.c \
../modules/tcc/tccelf.c \
../modules/tcc/tccgen.c \
../modules/tcc/tccpe.c \
../modules/tcc/tccpp.c \
../modules/tcc/tccrun.c \
../modules/tcc/x86_64-gen.c 

OBJS += \
./modules/tcc/arm-gen.o \
./modules/tcc/c67-gen.o \
./modules/tcc/conftest.o \
./modules/tcc/i386-asm.o \
./modules/tcc/i386-gen.o \
./modules/tcc/il-gen.o \
./modules/tcc/libtcc.o \
./modules/tcc/tcc.o \
./modules/tcc/tccasm.o \
./modules/tcc/tcccoff.o \
./modules/tcc/tccelf.o \
./modules/tcc/tccgen.o \
./modules/tcc/tccpe.o \
./modules/tcc/tccpp.o \
./modules/tcc/tccrun.o \
./modules/tcc/x86_64-gen.o 

C_DEPS += \
./modules/tcc/arm-gen.d \
./modules/tcc/c67-gen.d \
./modules/tcc/conftest.d \
./modules/tcc/i386-asm.d \
./modules/tcc/i386-gen.d \
./modules/tcc/il-gen.d \
./modules/tcc/libtcc.d \
./modules/tcc/tcc.d \
./modules/tcc/tccasm.d \
./modules/tcc/tcccoff.d \
./modules/tcc/tccelf.d \
./modules/tcc/tccgen.d \
./modules/tcc/tccpe.d \
./modules/tcc/tccpp.d \
./modules/tcc/tccrun.d \
./modules/tcc/x86_64-gen.d 


# Each subdirectory must supply rules for building sources it contributes
modules/tcc/%.o: ../modules/tcc/%.c modules/tcc/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


