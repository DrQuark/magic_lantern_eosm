################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/tcc/tests/tests2/00_assignment.c \
../modules/tcc/tests/tests2/01_comment.c \
../modules/tcc/tests/tests2/02_printf.c \
../modules/tcc/tests/tests2/03_struct.c \
../modules/tcc/tests/tests2/04_for.c \
../modules/tcc/tests/tests2/05_array.c \
../modules/tcc/tests/tests2/06_case.c \
../modules/tcc/tests/tests2/07_function.c \
../modules/tcc/tests/tests2/08_while.c \
../modules/tcc/tests/tests2/09_do_while.c \
../modules/tcc/tests/tests2/10_pointer.c \
../modules/tcc/tests/tests2/11_precedence.c \
../modules/tcc/tests/tests2/12_hashdefine.c \
../modules/tcc/tests/tests2/13_integer_literals.c \
../modules/tcc/tests/tests2/14_if.c \
../modules/tcc/tests/tests2/15_recursion.c \
../modules/tcc/tests/tests2/16_nesting.c \
../modules/tcc/tests/tests2/17_enum.c \
../modules/tcc/tests/tests2/18_include.c \
../modules/tcc/tests/tests2/19_pointer_arithmetic.c \
../modules/tcc/tests/tests2/20_pointer_comparison.c \
../modules/tcc/tests/tests2/21_char_array.c \
../modules/tcc/tests/tests2/22_floating_point.c \
../modules/tcc/tests/tests2/23_type_coercion.c \
../modules/tcc/tests/tests2/24_math_library.c \
../modules/tcc/tests/tests2/25_quicksort.c \
../modules/tcc/tests/tests2/26_character_constants.c \
../modules/tcc/tests/tests2/27_sizeof.c \
../modules/tcc/tests/tests2/28_strings.c \
../modules/tcc/tests/tests2/29_array_address.c \
../modules/tcc/tests/tests2/30_hanoi.c \
../modules/tcc/tests/tests2/31_args.c \
../modules/tcc/tests/tests2/32_led.c \
../modules/tcc/tests/tests2/33_ternary_op.c \
../modules/tcc/tests/tests2/34_array_assignment.c \
../modules/tcc/tests/tests2/35_sizeof.c \
../modules/tcc/tests/tests2/36_array_initialisers.c \
../modules/tcc/tests/tests2/37_sprintf.c \
../modules/tcc/tests/tests2/38_multiple_array_index.c \
../modules/tcc/tests/tests2/39_typedef.c \
../modules/tcc/tests/tests2/40_stdio.c \
../modules/tcc/tests/tests2/41_hashif.c \
../modules/tcc/tests/tests2/42_function_pointer.c \
../modules/tcc/tests/tests2/43_void_param.c \
../modules/tcc/tests/tests2/44_scoped_declarations.c \
../modules/tcc/tests/tests2/45_empty_for.c \
../modules/tcc/tests/tests2/46_grep.c \
../modules/tcc/tests/tests2/47_switch_return.c \
../modules/tcc/tests/tests2/48_nested_break.c \
../modules/tcc/tests/tests2/49_bracket_evaluation.c \
../modules/tcc/tests/tests2/50_logical_second_arg.c \
../modules/tcc/tests/tests2/51_static.c \
../modules/tcc/tests/tests2/52_unnamed_enum.c \
../modules/tcc/tests/tests2/54_goto.c \
../modules/tcc/tests/tests2/55_lshift_type.c 

OBJS += \
./modules/tcc/tests/tests2/00_assignment.o \
./modules/tcc/tests/tests2/01_comment.o \
./modules/tcc/tests/tests2/02_printf.o \
./modules/tcc/tests/tests2/03_struct.o \
./modules/tcc/tests/tests2/04_for.o \
./modules/tcc/tests/tests2/05_array.o \
./modules/tcc/tests/tests2/06_case.o \
./modules/tcc/tests/tests2/07_function.o \
./modules/tcc/tests/tests2/08_while.o \
./modules/tcc/tests/tests2/09_do_while.o \
./modules/tcc/tests/tests2/10_pointer.o \
./modules/tcc/tests/tests2/11_precedence.o \
./modules/tcc/tests/tests2/12_hashdefine.o \
./modules/tcc/tests/tests2/13_integer_literals.o \
./modules/tcc/tests/tests2/14_if.o \
./modules/tcc/tests/tests2/15_recursion.o \
./modules/tcc/tests/tests2/16_nesting.o \
./modules/tcc/tests/tests2/17_enum.o \
./modules/tcc/tests/tests2/18_include.o \
./modules/tcc/tests/tests2/19_pointer_arithmetic.o \
./modules/tcc/tests/tests2/20_pointer_comparison.o \
./modules/tcc/tests/tests2/21_char_array.o \
./modules/tcc/tests/tests2/22_floating_point.o \
./modules/tcc/tests/tests2/23_type_coercion.o \
./modules/tcc/tests/tests2/24_math_library.o \
./modules/tcc/tests/tests2/25_quicksort.o \
./modules/tcc/tests/tests2/26_character_constants.o \
./modules/tcc/tests/tests2/27_sizeof.o \
./modules/tcc/tests/tests2/28_strings.o \
./modules/tcc/tests/tests2/29_array_address.o \
./modules/tcc/tests/tests2/30_hanoi.o \
./modules/tcc/tests/tests2/31_args.o \
./modules/tcc/tests/tests2/32_led.o \
./modules/tcc/tests/tests2/33_ternary_op.o \
./modules/tcc/tests/tests2/34_array_assignment.o \
./modules/tcc/tests/tests2/35_sizeof.o \
./modules/tcc/tests/tests2/36_array_initialisers.o \
./modules/tcc/tests/tests2/37_sprintf.o \
./modules/tcc/tests/tests2/38_multiple_array_index.o \
./modules/tcc/tests/tests2/39_typedef.o \
./modules/tcc/tests/tests2/40_stdio.o \
./modules/tcc/tests/tests2/41_hashif.o \
./modules/tcc/tests/tests2/42_function_pointer.o \
./modules/tcc/tests/tests2/43_void_param.o \
./modules/tcc/tests/tests2/44_scoped_declarations.o \
./modules/tcc/tests/tests2/45_empty_for.o \
./modules/tcc/tests/tests2/46_grep.o \
./modules/tcc/tests/tests2/47_switch_return.o \
./modules/tcc/tests/tests2/48_nested_break.o \
./modules/tcc/tests/tests2/49_bracket_evaluation.o \
./modules/tcc/tests/tests2/50_logical_second_arg.o \
./modules/tcc/tests/tests2/51_static.o \
./modules/tcc/tests/tests2/52_unnamed_enum.o \
./modules/tcc/tests/tests2/54_goto.o \
./modules/tcc/tests/tests2/55_lshift_type.o 

C_DEPS += \
./modules/tcc/tests/tests2/00_assignment.d \
./modules/tcc/tests/tests2/01_comment.d \
./modules/tcc/tests/tests2/02_printf.d \
./modules/tcc/tests/tests2/03_struct.d \
./modules/tcc/tests/tests2/04_for.d \
./modules/tcc/tests/tests2/05_array.d \
./modules/tcc/tests/tests2/06_case.d \
./modules/tcc/tests/tests2/07_function.d \
./modules/tcc/tests/tests2/08_while.d \
./modules/tcc/tests/tests2/09_do_while.d \
./modules/tcc/tests/tests2/10_pointer.d \
./modules/tcc/tests/tests2/11_precedence.d \
./modules/tcc/tests/tests2/12_hashdefine.d \
./modules/tcc/tests/tests2/13_integer_literals.d \
./modules/tcc/tests/tests2/14_if.d \
./modules/tcc/tests/tests2/15_recursion.d \
./modules/tcc/tests/tests2/16_nesting.d \
./modules/tcc/tests/tests2/17_enum.d \
./modules/tcc/tests/tests2/18_include.d \
./modules/tcc/tests/tests2/19_pointer_arithmetic.d \
./modules/tcc/tests/tests2/20_pointer_comparison.d \
./modules/tcc/tests/tests2/21_char_array.d \
./modules/tcc/tests/tests2/22_floating_point.d \
./modules/tcc/tests/tests2/23_type_coercion.d \
./modules/tcc/tests/tests2/24_math_library.d \
./modules/tcc/tests/tests2/25_quicksort.d \
./modules/tcc/tests/tests2/26_character_constants.d \
./modules/tcc/tests/tests2/27_sizeof.d \
./modules/tcc/tests/tests2/28_strings.d \
./modules/tcc/tests/tests2/29_array_address.d \
./modules/tcc/tests/tests2/30_hanoi.d \
./modules/tcc/tests/tests2/31_args.d \
./modules/tcc/tests/tests2/32_led.d \
./modules/tcc/tests/tests2/33_ternary_op.d \
./modules/tcc/tests/tests2/34_array_assignment.d \
./modules/tcc/tests/tests2/35_sizeof.d \
./modules/tcc/tests/tests2/36_array_initialisers.d \
./modules/tcc/tests/tests2/37_sprintf.d \
./modules/tcc/tests/tests2/38_multiple_array_index.d \
./modules/tcc/tests/tests2/39_typedef.d \
./modules/tcc/tests/tests2/40_stdio.d \
./modules/tcc/tests/tests2/41_hashif.d \
./modules/tcc/tests/tests2/42_function_pointer.d \
./modules/tcc/tests/tests2/43_void_param.d \
./modules/tcc/tests/tests2/44_scoped_declarations.d \
./modules/tcc/tests/tests2/45_empty_for.d \
./modules/tcc/tests/tests2/46_grep.d \
./modules/tcc/tests/tests2/47_switch_return.d \
./modules/tcc/tests/tests2/48_nested_break.d \
./modules/tcc/tests/tests2/49_bracket_evaluation.d \
./modules/tcc/tests/tests2/50_logical_second_arg.d \
./modules/tcc/tests/tests2/51_static.d \
./modules/tcc/tests/tests2/52_unnamed_enum.d \
./modules/tcc/tests/tests2/54_goto.d \
./modules/tcc/tests/tests2/55_lshift_type.d 


# Each subdirectory must supply rules for building sources it contributes
modules/tcc/tests/tests2/%.o: ../modules/tcc/tests/tests2/%.c modules/tcc/tests/tests2/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


