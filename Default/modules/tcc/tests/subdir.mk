################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/tcc/tests/boundtest.c \
../modules/tcc/tests/libtcc_test.c \
../modules/tcc/tests/tcctest.c 

S_UPPER_SRCS += \
../modules/tcc/tests/asmtest.S 

OBJS += \
./modules/tcc/tests/asmtest.o \
./modules/tcc/tests/boundtest.o \
./modules/tcc/tests/libtcc_test.o \
./modules/tcc/tests/tcctest.o 

C_DEPS += \
./modules/tcc/tests/boundtest.d \
./modules/tcc/tests/libtcc_test.d \
./modules/tcc/tests/tcctest.d 


# Each subdirectory must supply rules for building sources it contributes
modules/tcc/tests/%.o: ../modules/tcc/tests/%.S modules/tcc/tests/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

modules/tcc/tests/%.o: ../modules/tcc/tests/%.c modules/tcc/tests/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


