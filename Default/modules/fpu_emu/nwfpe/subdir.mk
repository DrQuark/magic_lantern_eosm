################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/fpu_emu/nwfpe/double_cpdo.c \
../modules/fpu_emu/nwfpe/extended_cpdo.c \
../modules/fpu_emu/nwfpe/fpa11.c \
../modules/fpu_emu/nwfpe/fpa11_cpdo.c \
../modules/fpu_emu/nwfpe/fpa11_cpdt.c \
../modules/fpu_emu/nwfpe/fpa11_cprt.c \
../modules/fpu_emu/nwfpe/fpmodule.c \
../modules/fpu_emu/nwfpe/fpopcode.c \
../modules/fpu_emu/nwfpe/single_cpdo.c \
../modules/fpu_emu/nwfpe/softfloat.c 

OBJS += \
./modules/fpu_emu/nwfpe/double_cpdo.o \
./modules/fpu_emu/nwfpe/extended_cpdo.o \
./modules/fpu_emu/nwfpe/fpa11.o \
./modules/fpu_emu/nwfpe/fpa11_cpdo.o \
./modules/fpu_emu/nwfpe/fpa11_cpdt.o \
./modules/fpu_emu/nwfpe/fpa11_cprt.o \
./modules/fpu_emu/nwfpe/fpmodule.o \
./modules/fpu_emu/nwfpe/fpopcode.o \
./modules/fpu_emu/nwfpe/single_cpdo.o \
./modules/fpu_emu/nwfpe/softfloat.o 

C_DEPS += \
./modules/fpu_emu/nwfpe/double_cpdo.d \
./modules/fpu_emu/nwfpe/extended_cpdo.d \
./modules/fpu_emu/nwfpe/fpa11.d \
./modules/fpu_emu/nwfpe/fpa11_cpdo.d \
./modules/fpu_emu/nwfpe/fpa11_cpdt.d \
./modules/fpu_emu/nwfpe/fpa11_cprt.d \
./modules/fpu_emu/nwfpe/fpmodule.d \
./modules/fpu_emu/nwfpe/fpopcode.d \
./modules/fpu_emu/nwfpe/single_cpdo.d \
./modules/fpu_emu/nwfpe/softfloat.d 


# Each subdirectory must supply rules for building sources it contributes
modules/fpu_emu/nwfpe/%.o: ../modules/fpu_emu/nwfpe/%.c modules/fpu_emu/nwfpe/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


