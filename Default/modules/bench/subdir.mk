################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/bench/bench.c \
../modules/bench/card_bench.c \
../modules/bench/mem_bench.c \
../modules/bench/mem_perf.c 

O_SRCS += \
../modules/bench/bench.o 

OBJS += \
./modules/bench/bench.o \
./modules/bench/card_bench.o \
./modules/bench/mem_bench.o \
./modules/bench/mem_perf.o 

C_DEPS += \
./modules/bench/bench.d \
./modules/bench/card_bench.d \
./modules/bench/mem_bench.d \
./modules/bench/mem_perf.d 


# Each subdirectory must supply rules for building sources it contributes
modules/bench/%.o: ../modules/bench/%.c modules/bench/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


