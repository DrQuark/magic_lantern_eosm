################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../modules/io_crypt/bigd.c \
../modules/io_crypt/bigdigits.c \
../modules/io_crypt/crypt_lfsr64.c \
../modules/io_crypt/crypt_rsa.c \
../modules/io_crypt/crypt_xtea.c \
../modules/io_crypt/hash_password.c \
../modules/io_crypt/io_crypt.c \
../modules/io_crypt/io_decrypt.c 

OBJS += \
./modules/io_crypt/bigd.o \
./modules/io_crypt/bigdigits.o \
./modules/io_crypt/crypt_lfsr64.o \
./modules/io_crypt/crypt_rsa.o \
./modules/io_crypt/crypt_xtea.o \
./modules/io_crypt/hash_password.o \
./modules/io_crypt/io_crypt.o \
./modules/io_crypt/io_decrypt.o 

C_DEPS += \
./modules/io_crypt/bigd.d \
./modules/io_crypt/bigdigits.d \
./modules/io_crypt/crypt_lfsr64.d \
./modules/io_crypt/crypt_rsa.d \
./modules/io_crypt/crypt_xtea.d \
./modules/io_crypt/hash_password.d \
./modules/io_crypt/io_crypt.d \
./modules/io_crypt/io_decrypt.d 


# Each subdirectory must supply rules for building sources it contributes
modules/io_crypt/%.o: ../modules/io_crypt/%.c modules/io_crypt/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


