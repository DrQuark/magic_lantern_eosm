################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../tcc/tests/boundtest.c \
../tcc/tests/libtcc_test.c \
../tcc/tests/tcctest.c 

S_UPPER_SRCS += \
../tcc/tests/asmtest.S 

OBJS += \
./tcc/tests/asmtest.o \
./tcc/tests/boundtest.o \
./tcc/tests/libtcc_test.o \
./tcc/tests/tcctest.o 

C_DEPS += \
./tcc/tests/boundtest.d \
./tcc/tests/libtcc_test.d \
./tcc/tests/tcctest.d 


# Each subdirectory must supply rules for building sources it contributes
tcc/tests/%.o: ../tcc/tests/%.S tcc/tests/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

tcc/tests/%.o: ../tcc/tests/%.c tcc/tests/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


