################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../tcc/tests/tests2/00_assignment.c \
../tcc/tests/tests2/01_comment.c \
../tcc/tests/tests2/02_printf.c \
../tcc/tests/tests2/03_struct.c \
../tcc/tests/tests2/04_for.c \
../tcc/tests/tests2/05_array.c \
../tcc/tests/tests2/06_case.c \
../tcc/tests/tests2/07_function.c \
../tcc/tests/tests2/08_while.c \
../tcc/tests/tests2/09_do_while.c \
../tcc/tests/tests2/10_pointer.c \
../tcc/tests/tests2/11_precedence.c \
../tcc/tests/tests2/12_hashdefine.c \
../tcc/tests/tests2/13_integer_literals.c \
../tcc/tests/tests2/14_if.c \
../tcc/tests/tests2/15_recursion.c \
../tcc/tests/tests2/16_nesting.c \
../tcc/tests/tests2/17_enum.c \
../tcc/tests/tests2/18_include.c \
../tcc/tests/tests2/19_pointer_arithmetic.c \
../tcc/tests/tests2/20_pointer_comparison.c \
../tcc/tests/tests2/21_char_array.c \
../tcc/tests/tests2/22_floating_point.c \
../tcc/tests/tests2/23_type_coercion.c \
../tcc/tests/tests2/24_math_library.c \
../tcc/tests/tests2/25_quicksort.c \
../tcc/tests/tests2/26_character_constants.c \
../tcc/tests/tests2/27_sizeof.c \
../tcc/tests/tests2/28_strings.c \
../tcc/tests/tests2/29_array_address.c \
../tcc/tests/tests2/30_hanoi.c \
../tcc/tests/tests2/31_args.c \
../tcc/tests/tests2/32_led.c \
../tcc/tests/tests2/33_ternary_op.c \
../tcc/tests/tests2/34_array_assignment.c \
../tcc/tests/tests2/35_sizeof.c \
../tcc/tests/tests2/36_array_initialisers.c \
../tcc/tests/tests2/37_sprintf.c \
../tcc/tests/tests2/38_multiple_array_index.c \
../tcc/tests/tests2/39_typedef.c \
../tcc/tests/tests2/40_stdio.c \
../tcc/tests/tests2/41_hashif.c \
../tcc/tests/tests2/42_function_pointer.c \
../tcc/tests/tests2/43_void_param.c \
../tcc/tests/tests2/44_scoped_declarations.c \
../tcc/tests/tests2/45_empty_for.c \
../tcc/tests/tests2/46_grep.c \
../tcc/tests/tests2/47_switch_return.c \
../tcc/tests/tests2/48_nested_break.c \
../tcc/tests/tests2/49_bracket_evaluation.c \
../tcc/tests/tests2/50_logical_second_arg.c \
../tcc/tests/tests2/51_static.c \
../tcc/tests/tests2/52_unnamed_enum.c \
../tcc/tests/tests2/54_goto.c \
../tcc/tests/tests2/55_lshift_type.c 

OBJS += \
./tcc/tests/tests2/00_assignment.o \
./tcc/tests/tests2/01_comment.o \
./tcc/tests/tests2/02_printf.o \
./tcc/tests/tests2/03_struct.o \
./tcc/tests/tests2/04_for.o \
./tcc/tests/tests2/05_array.o \
./tcc/tests/tests2/06_case.o \
./tcc/tests/tests2/07_function.o \
./tcc/tests/tests2/08_while.o \
./tcc/tests/tests2/09_do_while.o \
./tcc/tests/tests2/10_pointer.o \
./tcc/tests/tests2/11_precedence.o \
./tcc/tests/tests2/12_hashdefine.o \
./tcc/tests/tests2/13_integer_literals.o \
./tcc/tests/tests2/14_if.o \
./tcc/tests/tests2/15_recursion.o \
./tcc/tests/tests2/16_nesting.o \
./tcc/tests/tests2/17_enum.o \
./tcc/tests/tests2/18_include.o \
./tcc/tests/tests2/19_pointer_arithmetic.o \
./tcc/tests/tests2/20_pointer_comparison.o \
./tcc/tests/tests2/21_char_array.o \
./tcc/tests/tests2/22_floating_point.o \
./tcc/tests/tests2/23_type_coercion.o \
./tcc/tests/tests2/24_math_library.o \
./tcc/tests/tests2/25_quicksort.o \
./tcc/tests/tests2/26_character_constants.o \
./tcc/tests/tests2/27_sizeof.o \
./tcc/tests/tests2/28_strings.o \
./tcc/tests/tests2/29_array_address.o \
./tcc/tests/tests2/30_hanoi.o \
./tcc/tests/tests2/31_args.o \
./tcc/tests/tests2/32_led.o \
./tcc/tests/tests2/33_ternary_op.o \
./tcc/tests/tests2/34_array_assignment.o \
./tcc/tests/tests2/35_sizeof.o \
./tcc/tests/tests2/36_array_initialisers.o \
./tcc/tests/tests2/37_sprintf.o \
./tcc/tests/tests2/38_multiple_array_index.o \
./tcc/tests/tests2/39_typedef.o \
./tcc/tests/tests2/40_stdio.o \
./tcc/tests/tests2/41_hashif.o \
./tcc/tests/tests2/42_function_pointer.o \
./tcc/tests/tests2/43_void_param.o \
./tcc/tests/tests2/44_scoped_declarations.o \
./tcc/tests/tests2/45_empty_for.o \
./tcc/tests/tests2/46_grep.o \
./tcc/tests/tests2/47_switch_return.o \
./tcc/tests/tests2/48_nested_break.o \
./tcc/tests/tests2/49_bracket_evaluation.o \
./tcc/tests/tests2/50_logical_second_arg.o \
./tcc/tests/tests2/51_static.o \
./tcc/tests/tests2/52_unnamed_enum.o \
./tcc/tests/tests2/54_goto.o \
./tcc/tests/tests2/55_lshift_type.o 

C_DEPS += \
./tcc/tests/tests2/00_assignment.d \
./tcc/tests/tests2/01_comment.d \
./tcc/tests/tests2/02_printf.d \
./tcc/tests/tests2/03_struct.d \
./tcc/tests/tests2/04_for.d \
./tcc/tests/tests2/05_array.d \
./tcc/tests/tests2/06_case.d \
./tcc/tests/tests2/07_function.d \
./tcc/tests/tests2/08_while.d \
./tcc/tests/tests2/09_do_while.d \
./tcc/tests/tests2/10_pointer.d \
./tcc/tests/tests2/11_precedence.d \
./tcc/tests/tests2/12_hashdefine.d \
./tcc/tests/tests2/13_integer_literals.d \
./tcc/tests/tests2/14_if.d \
./tcc/tests/tests2/15_recursion.d \
./tcc/tests/tests2/16_nesting.d \
./tcc/tests/tests2/17_enum.d \
./tcc/tests/tests2/18_include.d \
./tcc/tests/tests2/19_pointer_arithmetic.d \
./tcc/tests/tests2/20_pointer_comparison.d \
./tcc/tests/tests2/21_char_array.d \
./tcc/tests/tests2/22_floating_point.d \
./tcc/tests/tests2/23_type_coercion.d \
./tcc/tests/tests2/24_math_library.d \
./tcc/tests/tests2/25_quicksort.d \
./tcc/tests/tests2/26_character_constants.d \
./tcc/tests/tests2/27_sizeof.d \
./tcc/tests/tests2/28_strings.d \
./tcc/tests/tests2/29_array_address.d \
./tcc/tests/tests2/30_hanoi.d \
./tcc/tests/tests2/31_args.d \
./tcc/tests/tests2/32_led.d \
./tcc/tests/tests2/33_ternary_op.d \
./tcc/tests/tests2/34_array_assignment.d \
./tcc/tests/tests2/35_sizeof.d \
./tcc/tests/tests2/36_array_initialisers.d \
./tcc/tests/tests2/37_sprintf.d \
./tcc/tests/tests2/38_multiple_array_index.d \
./tcc/tests/tests2/39_typedef.d \
./tcc/tests/tests2/40_stdio.d \
./tcc/tests/tests2/41_hashif.d \
./tcc/tests/tests2/42_function_pointer.d \
./tcc/tests/tests2/43_void_param.d \
./tcc/tests/tests2/44_scoped_declarations.d \
./tcc/tests/tests2/45_empty_for.d \
./tcc/tests/tests2/46_grep.d \
./tcc/tests/tests2/47_switch_return.d \
./tcc/tests/tests2/48_nested_break.d \
./tcc/tests/tests2/49_bracket_evaluation.d \
./tcc/tests/tests2/50_logical_second_arg.d \
./tcc/tests/tests2/51_static.d \
./tcc/tests/tests2/52_unnamed_enum.d \
./tcc/tests/tests2/54_goto.d \
./tcc/tests/tests2/55_lshift_type.d 


# Each subdirectory must supply rules for building sources it contributes
tcc/tests/tests2/%.o: ../tcc/tests/tests2/%.c tcc/tests/tests2/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


