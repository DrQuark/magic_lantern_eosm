################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../contrib/qemu/eos/engine.c \
../contrib/qemu/eos/eos.c \
../contrib/qemu/eos/eos_ml_helpers.c \
../contrib/qemu/eos/model_list.c \
../contrib/qemu/eos/mpu.c \
../contrib/qemu/eos/serial_flash.c 

OBJS += \
./contrib/qemu/eos/engine.o \
./contrib/qemu/eos/eos.o \
./contrib/qemu/eos/eos_ml_helpers.o \
./contrib/qemu/eos/model_list.o \
./contrib/qemu/eos/mpu.o \
./contrib/qemu/eos/serial_flash.o 

C_DEPS += \
./contrib/qemu/eos/engine.d \
./contrib/qemu/eos/eos.d \
./contrib/qemu/eos/eos_ml_helpers.d \
./contrib/qemu/eos/model_list.d \
./contrib/qemu/eos/mpu.d \
./contrib/qemu/eos/serial_flash.d 


# Each subdirectory must supply rules for building sources it contributes
contrib/qemu/eos/%.o: ../contrib/qemu/eos/%.c contrib/qemu/eos/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


