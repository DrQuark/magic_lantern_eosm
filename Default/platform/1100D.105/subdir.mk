################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/1100D.105/cfn.c 

S_UPPER_SRCS += \
../platform/1100D.105/stubs.S 

OBJS += \
./platform/1100D.105/cfn.o \
./platform/1100D.105/stubs.o 

C_DEPS += \
./platform/1100D.105/cfn.d 


# Each subdirectory must supply rules for building sources it contributes
platform/1100D.105/%.o: ../platform/1100D.105/%.c platform/1100D.105/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

platform/1100D.105/%.o: ../platform/1100D.105/%.S platform/1100D.105/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


