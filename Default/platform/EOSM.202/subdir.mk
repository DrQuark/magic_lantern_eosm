################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/EOSM.202/cfn.c \
../platform/EOSM.202/version.c 

O_SRCS += \
../platform/EOSM.202/atoi.o \
../platform/EOSM.202/atol.o \
../platform/EOSM.202/audio-ak.o \
../platform/EOSM.202/backtrace.o \
../platform/EOSM.202/battery.o \
../platform/EOSM.202/beep.o \
../platform/EOSM.202/bitrate.o \
../platform/EOSM.202/bmp.o \
../platform/EOSM.202/boot-hack.o \
../platform/EOSM.202/bootflags.o \
../platform/EOSM.202/builtin-enforcing.o \
../platform/EOSM.202/cache.o \
../platform/EOSM.202/cfn.o \
../platform/EOSM.202/chdk-dng.o \
../platform/EOSM.202/chdk-gui_draw.o \
../platform/EOSM.202/config.o \
../platform/EOSM.202/console.o \
../platform/EOSM.202/crop-mode-hack.o \
../platform/EOSM.202/debug.o \
../platform/EOSM.202/dialog_test.o \
../platform/EOSM.202/disp_direct.o \
../platform/EOSM.202/edmac-memcpy.o \
../platform/EOSM.202/edmac.o \
../platform/EOSM.202/electronic_level.o \
../platform/EOSM.202/entry.o \
../platform/EOSM.202/errno.o \
../platform/EOSM.202/errno_location.o \
../platform/EOSM.202/exmem.o \
../platform/EOSM.202/falsecolor.o \
../platform/EOSM.202/fileprefix.o \
../platform/EOSM.202/fio-ml.o \
../platform/EOSM.202/flexinfo.o \
../platform/EOSM.202/focus.o \
../platform/EOSM.202/font_direct.o \
../platform/EOSM.202/footer.o \
../platform/EOSM.202/fps-engio.o \
../platform/EOSM.202/greenscreen.o \
../platform/EOSM.202/gui-common.o \
../platform/EOSM.202/gui.o \
../platform/EOSM.202/hdr.o \
../platform/EOSM.202/histogram.o \
../platform/EOSM.202/ico.o \
../platform/EOSM.202/imath.o \
../platform/EOSM.202/imgconv.o \
../platform/EOSM.202/lcdsensor.o \
../platform/EOSM.202/lens.o \
../platform/EOSM.202/lib_a-memccpy.o \
../platform/EOSM.202/lib_a-memcpy-stub.o \
../platform/EOSM.202/lib_a-memcpy.o \
../platform/EOSM.202/lib_a-memmove.o \
../platform/EOSM.202/lib_a-memset.o \
../platform/EOSM.202/lib_a-setjmp.o \
../platform/EOSM.202/lv-img-engio.o \
../platform/EOSM.202/lvinfo.o \
../platform/EOSM.202/mem.o \
../platform/EOSM.202/memcmp.o \
../platform/EOSM.202/memmove.o \
../platform/EOSM.202/menu.o \
../platform/EOSM.202/menuhelp.o \
../platform/EOSM.202/menuindex.o \
../platform/EOSM.202/ml-cbr.o \
../platform/EOSM.202/module.o \
../platform/EOSM.202/movie_menu_raw_only.o \
../platform/EOSM.202/movtweaks.o \
../platform/EOSM.202/notify_box.o \
../platform/EOSM.202/patch.o \
../platform/EOSM.202/ph_info_disp.o \
../platform/EOSM.202/picstyle.o \
../platform/EOSM.202/posix.o \
../platform/EOSM.202/powersave.o \
../platform/EOSM.202/property.o \
../platform/EOSM.202/propvalues.o \
../platform/EOSM.202/rand.o \
../platform/EOSM.202/raw.o \
../platform/EOSM.202/rbf_font.o \
../platform/EOSM.202/reboot.o \
../platform/EOSM.202/screenshot.o \
../platform/EOSM.202/shoot.o \
../platform/EOSM.202/state-object.o \
../platform/EOSM.202/stdio.o \
../platform/EOSM.202/strcasecmp.o \
../platform/EOSM.202/strchr.o \
../platform/EOSM.202/strcmp.o \
../platform/EOSM.202/strcpy.o \
../platform/EOSM.202/strlen.o \
../platform/EOSM.202/strncmp.o \
../platform/EOSM.202/strncpy.o \
../platform/EOSM.202/strrchr.o \
../platform/EOSM.202/strstr.o \
../platform/EOSM.202/strtol.o \
../platform/EOSM.202/strtoul.o \
../platform/EOSM.202/strtoull.o \
../platform/EOSM.202/stubs.o \
../platform/EOSM.202/tasks.o \
../platform/EOSM.202/tcc-glue.o \
../platform/EOSM.202/tskmon.o \
../platform/EOSM.202/tweaks-eyefi.o \
../platform/EOSM.202/tweaks.o \
../platform/EOSM.202/util.o \
../platform/EOSM.202/vectorscope.o \
../platform/EOSM.202/version.o \
../platform/EOSM.202/vram.o \
../platform/EOSM.202/vsync-lite.o \
../platform/EOSM.202/zebra.o 

S_UPPER_SRCS += \
../platform/EOSM.202/stubs.S 

OBJS += \
./platform/EOSM.202/cfn.o \
./platform/EOSM.202/stubs.o \
./platform/EOSM.202/version.o 

C_DEPS += \
./platform/EOSM.202/cfn.d \
./platform/EOSM.202/version.d 


# Each subdirectory must supply rules for building sources it contributes
platform/EOSM.202/%.o: ../platform/EOSM.202/%.c platform/EOSM.202/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DECLIPSE_IDE=1 -I"/home/cedric/sources/GIT/magic-lantern_jip_hop_git/platform/EOSM.202" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

platform/EOSM.202/%.o: ../platform/EOSM.202/%.S platform/EOSM.202/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


