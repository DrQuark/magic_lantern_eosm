################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/500D.111/audio.c.500d_under_construction.c \
../platform/500D.111/cfn.c \
../platform/500D.111/gui.c 

S_UPPER_SRCS += \
../platform/500D.111/stubs.S 

OBJS += \
./platform/500D.111/audio.c.500d_under_construction.o \
./platform/500D.111/cfn.o \
./platform/500D.111/gui.o \
./platform/500D.111/stubs.o 

C_DEPS += \
./platform/500D.111/audio.c.500d_under_construction.d \
./platform/500D.111/cfn.d \
./platform/500D.111/gui.d 


# Each subdirectory must supply rules for building sources it contributes
platform/500D.111/%.o: ../platform/500D.111/%.c platform/500D.111/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

platform/500D.111/%.o: ../platform/500D.111/%.S platform/500D.111/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


