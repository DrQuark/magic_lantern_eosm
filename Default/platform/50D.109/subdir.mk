################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/50D.109/cfn.c \
../platform/50D.109/gui.c \
../platform/50D.109/misc.c 

S_UPPER_SRCS += \
../platform/50D.109/stubs.S 

OBJS += \
./platform/50D.109/cfn.o \
./platform/50D.109/gui.o \
./platform/50D.109/misc.o \
./platform/50D.109/stubs.o 

C_DEPS += \
./platform/50D.109/cfn.d \
./platform/50D.109/gui.d \
./platform/50D.109/misc.d 


# Each subdirectory must supply rules for building sources it contributes
platform/50D.109/%.o: ../platform/50D.109/%.c platform/50D.109/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

platform/50D.109/%.o: ../platform/50D.109/%.S platform/50D.109/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


