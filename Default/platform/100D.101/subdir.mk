################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/100D.101/cfn.c 

S_UPPER_SRCS += \
../platform/100D.101/stubs.S 

OBJS += \
./platform/100D.101/cfn.o \
./platform/100D.101/stubs.o 

C_DEPS += \
./platform/100D.101/cfn.d 


# Each subdirectory must supply rules for building sources it contributes
platform/100D.101/%.o: ../platform/100D.101/%.c platform/100D.101/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

platform/100D.101/%.o: ../platform/100D.101/%.S platform/100D.101/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


