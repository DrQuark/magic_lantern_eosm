#ifndef __ptp_extra_h
#define __ptp_extra_h

#define PTP_EXTRA_VERSION_MAJOR 0  // increase only with backwards incompatible changes (and reset minor)
#define PTP_EXTRA_VERSION_MINOR 1  // increase with extensions of functionality

#define PTP_EXTRA_CODE 0xCEDA

// parameter count; data direction; return parameter values
enum {
	PTP_EXTRA_HelloWorld    = 0x0000,
	PTP_EXTRA_TakePicture        = 0x0001,
	PTP_EXTRA_GetLV      = 0x0002,
} ptp_ml_command;

#endif